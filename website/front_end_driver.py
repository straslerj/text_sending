import os

from flask import Flask, render_template, request

app = Flask(__name__)


@app.route("/", methods=["GET", "POST"])
def home():
    # get data from form for subscribing
    lake = request.args.get("lakes")
    email = request.args.get("email")
    phone_number = request.args.get("phone_num")
    carrier = request.args.get("carrier")

    print(os.getcwd())
    if os.getcwd()[-7:] == "website":
        os.chdir("..")
    file_path = "subscribers.txt"
    add_user(lake, email, phone_number, carrier, file_path)

    # get data from form for unsubscribing
    to_be_unsubbed = request.args.get("unsub_num")

    if to_be_unsubbed is not None:
        remove_user(file_path, to_be_unsubbed)

    return render_template("home.html")


# This method adds users who have opted in to receiving alerts.
def add_user(lake, email, phone_number, carrier, file_path):
    if lake is not None and email is not None and phone_number is not None and carrier is not None:
        file = open(file_path, "a+")
        file.write(str(lake) + "," + str(email) + "," + str(phone_number) + "," + str(carrier) + "\n")
        file.close()
        print(f"User {phone_number} added.")


# This method removes users who have opted out of receiving alerts.
def remove_user(file_path, user_to_remove):
    str(user_to_remove)
    with open(file_path, 'r') as file:
        lines = file.readlines()

        with open(file_path, 'w') as fw:
            for line in lines:
                if line.split(',', 3)[2] != str(user_to_remove):
                    fw.write(line)
                else:
                    print(f"User {user_to_remove} removed.")


if __name__ == "__main__":
    app.run(debug=False)

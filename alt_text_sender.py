import yagmail
from text_sender import add_carrier
from http_requests import request_all_lake_subs
from datetime import datetime


def alt_send_alert(lake_name, warning, message):
    # Email login
    email = "botdelivery0001@gmail.com"
    password = "botSender4000"

    # Put in Credentials
    yag = yagmail.SMTP(email, password)

    subscriber_dict = request_all_lake_subs()
    add_carrier(subscriber_dict)

    # Setting date formatters
    sent_time_date = datetime.now().strftime("%m/%d/%Y")
    sent_time_time = datetime.now().strftime("%H:%M:%S")
    formatted_time = datetime.now().strftime("%m/%d/%Y %H:%M:%S")

    # Send emails
    for user in subscriber_dict.values():
        if user["lake"] == lake_name:
            contents = [f"Algae {warning} for Lake {lake_name}.\n{message}\n\nSent on {sent_time_date} at {sent_time_time}"]
            yag.send(user['email'], 'Algae Bloom Alert', contents)
            print(f"Email sent at {formatted_time}")

    # Send texts
    for user in subscriber_dict.values():
        if user["lake"] == lake_name:
            contents = [f"Algae {warning} for Lake {lake_name}.\n{message}\n\nSent on {sent_time_date} at {sent_time_time}"]
            yag.send(user['phone'], 'Algae Bloom Alert', contents)
            print(f"Text sent at {formatted_time}")


if __name__ == "__main__":
    alt_send_alert()

import requests
from flask import Flask, render_template, request, make_response

FORECAST_SERVER_URL = "http://localhost:5001/update_forecast/"

app = Flask(__name__)

@app.route("/")
def home():
    return render_template("home.html")

@app.route("/update", methods= ["POST"])
def update():
        lake = (request.form['lake'].lower())
        warning = request.form['warning']
        message = request.form['message']
        url = FORECAST_SERVER_URL + lake + "/" + warning + "/" + message

        res = requests.post(url)

        return make_response(render_template("home.html"))


if __name__ == "__main__":
    app.run(host='localhost', port=5002)

from flask import Flask, request
from flask_restful import Resource, Api, reqparse, fields, marshal_with

import csv

app = Flask(__name__)
api = Api(app)

def db_read_subs(filename='subscribers.txt'):
    lakes = []

    # https://www.geeksforgeeks.org/reading-csv-files-in-python/
    with open(filename, mode='r') as subfile:
        csvsubs = csv.reader(subfile)
        for line in csvsubs:
            # Each line is as follows  [ (0): LakeName, (1): Email, (2): phone, (3): Carrier ]
            sub = Subscriber(line[1], line[2], line[3])

            makelake = True
            for lake in lakes:
                if lake.name == line[0]:
                    lake.add_sub(sub)
                    makelake = False
            
            if makelake:
                subs = [sub]
                lakes.append(Lake(line[0], subs))

        subfile.close()

    return lakes

class Subscriber:
    def __init__(self, email, phone, carrier):
        self.email = email
        self.phone = phone
        self.carrier = carrier

    def get_json(self):
        subdata = {
            'email': self.email,
            'phone': self.phone,
            'carrier': self.carrier
        }
        return subdata

class Lake:
    def __init__(self, name, subscribers=None):
        self.name = name
        if subscribers != None:
            self.subs = subscribers
        else:
            self.subs = {}

    def add_sub(self, subscriber):
        self.subs.append(subscriber)
    
    def remove_sub(self, subphone):
        for sub in self.subs:
            if sub.phone == subphone:
                self.subs.remove(sub)

    def get_json(self):
        # Make list of dicts, each dict is from sub.get_json
        subs_json = []
        for sub in self.subs:
            subs_json.append(sub.get_json())

        lakedata = {
            'lake': self.name,
            'subscribers': subs_json
        }
        return lakedata

class LakesResource(Resource):
    def get(self):
        lakes = db_read_subs()
        lakenames = {}
        i = 0
        for lake in lakes:
            lakenames[i] = lake.name
            i += 1

        return lakenames
        

api.add_resource(LakesResource, '/lakes', '/lake_subscribers')

# As the subscriber email list is read-only from the Alert System, only implementing gets REST api
class LakeResource(Resource):
    def get(self, lakename):
        lakes = db_read_subs()
        for lake in lakes:
            if lake.name == lakename:
                return lake.get_json(), 200
        return {'error': 'lake does not exist'}, 404

api.add_resource(LakeResource, '/lake_subscribers/<string:lakename>')

class WelcomeResource(Resource):
    def get(self):
        return {'Usage': 'Send a GET request to the following address: <WEBSITE>/lake_subscribers/<LAKENAME>, or to <WEBSITE>/lakes'}

api.add_resource(WelcomeResource, '/')

if __name__ == '__main__':
    app.run(debug=False, port=4999)
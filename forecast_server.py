from flask import Flask, request
from flask_restful import Resource, Api, reqparse, fields, marshal_with


app = Flask(__name__)
api = Api(app)

forecasts = {
    'alpha': {'warning':'Warning' , 'message':'Lake Alpha has an algae warning'},
    'beta': {'warning':'Watch' , 'message':'Lake Beta has an algae watch'},
    'gamma': {'warning':'Safe' , 'message':'Lake Gamma is safe'},
    'delta': {'warning':'Safe' , 'message':'Lake Delta is safe'}
}

class LakeForecast(Resource):
    def get(self, lakename):
        return forecasts.get(lakename)


api.add_resource(LakeForecast, '/forecast/<string:lakename>')

class ForecastUpdate(Resource):
    def post(self, lakename, warning, message):
        if not forecasts.get(lakename):
            return {'error': 'Lake does not exist'}

        forecasts.get(lakename)['warning'] = warning
        forecasts.get(lakename)['message'] = message

        return {}

api.add_resource(ForecastUpdate, '/update_forecast/<string:lakename>/<string:warning>/<string:message>')



class WelcomeResource(Resource):
    def get(self):
        return {
            'Usage': 'Send a GET request to the following address: <WEBSITE>/forecast/<LAKENAME>.'+
                     'Send a POST request to the following address (demo only): <WEBSITE>/update_forecast/<LAKENAME>/<WARNING>/<MESSAGE>.'}


api.add_resource(WelcomeResource, '/')





if __name__ == '__main__':
    app.run(debug=False, host='localhost', port=5001)


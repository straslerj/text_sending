import requests

from database_server import Subscriber

# https://www.w3schools.com/python/module_requests.asp

DATABASE_ADDR = "http://localhost:4999"
FORECAST_ADDR = "http://localhost:5001/forecast/"

LAKES_URL = DATABASE_ADDR + "/lakes"
LAKESUB_URL = DATABASE_ADDR + "/lake_subscribers"

# Request the subscriber data (see database diagram) for a given lake 
# DICT {
#  phone:
#       {
#           lake: <Lake's name>
#           email: <Subscriber's email address>
#           phone: <Subscriber's phone number>
#           carrier: <Subscriber's phone carrier>
#       }
# }
def request_lake_subs(lakename = ""):
    lake_subs = {}

    response = requests.get(LAKESUB_URL + "/" + lakename)
    json = response.json()
    # {'lake': 'Alpha', 'subscribers': [
    #   {'email': 'klucks@msoe.edu', 'phone': '2628991846', 'carrier': 'verizon'}, 
    #   {'email': 'mock1@gmail.com', 'phone': '123', 'carrier': 'att'}
    # ]}

    js_subs = json['subscribers']
    for sub in js_subs:
        sub['phone'] = str(sub['phone'])
        sub['lake'] = json['lake']
        lake_subs[int(sub['phone'])] = sub

    return lake_subs

# Requests the subscriber data (see database diagram) for all lakes in the database 
def request_all_lake_subs():
    lake_list = lakes_dicttolist(request_all_lakes())
    lake_subs_dict = {}
    for lake_name in lake_list:
        lake_subs = request_lake_subs(lake_name)
        lake_subs_dict.update(lake_subs)

    return lake_subs_dict

#  Requests a list of all lakes by name and their associated IDs 
def request_all_lakes():
    response = requests.get(LAKES_URL)
    json = response.json()
    return json

# Helper function turns lake_json dict to a list of lake names
def lakes_dicttolist(lake_json):
    lake_list = []
    for lake in lake_json:
        lake_list.append(lake_json[lake])
    return lake_list

# Requests any available warnings for all lakes in the system
def request_warnings():
    lakewrnings = {}
    lakelist = lakes_dicttolist(request_all_lakes())
    for lake in lakelist:
        json = (requests.get(FORECAST_ADDR + (lake.lower()))).json()
        #print('Response for lake %s %s' %(lake, json))
        lakewrnings[lake] = json

    return lakewrnings
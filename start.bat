@ECHO off

start /B python forecast_server.py
start /B python database_server.py

start /B python website\front_end_driver.py
start /B python forecast_demo_website\forecast_demo_site.py

start http://localhost:4999/
start http://localhost:5001/
start http://localhost:5000/
start http://localhost:5002/
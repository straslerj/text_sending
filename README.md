# Run instructions
1. Run the included start.bat file to start all backend servers and demo sites
    - Includes database mock, forecast mock, subscription site, and alert update site
2. Navigate to http://localhost:5000/ to access the subscription site
    - Follow site instructions to add/remove subscribers
3. Navigate to http://localhost:5002/ to access the alert update site
    - Input 'alpha', 'beta', 'gamma', or 'delta' for the lake, a warning state, and a message, then submit to change the status of a lake on the backend
4. When subscribers and alert states are set, run the below command, replacing 'x' with '1' or '2' to use a different text sending library
~~~
python alert_driver.py -s x
~~~ 

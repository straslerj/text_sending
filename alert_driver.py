from text_sender import send_alert
from alt_text_sender import alt_send_alert
from http_requests import request_all_lakes, lakes_dicttolist, request_warnings

import sys, getopt, argparse

MSGSNDR_OPTS = ['1', '2']

def main():
    parser = argparse.ArgumentParser(description='Send Algae Alerts.', add_help=True)
    parser.add_argument('-s', choices=MSGSNDR_OPTS, required=True, nargs=1, default=1, help=('Which message sender component to use, select from ' + str(MSGSNDR_OPTS)))

    args = vars(parser.parse_args())
    selected_sender = int(args['s'])

    warnings = request_warnings()
    lakes = lakes_dicttolist(request_all_lakes())

    for lake in lakes:
        if warnings[lake]['warning'].lower() == "warning" or warnings[lake]['warning'].lower() == "watch":
            if selected_sender == 1:
                send_alert(lake, warnings[lake]['warning'], warnings[lake]['message'])
            elif selected_sender == 2:
                alt_send_alert(lake, warnings[lake]['warning'], warnings[lake]['message'])

if __name__ == "__main__":
    main()